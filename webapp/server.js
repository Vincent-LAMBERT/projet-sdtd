const https = require('http');
const express = require('express');
const path = require('path');
const app = express();
//app.use(express.json());
app.use(express.static(__dirname + "/express"));// default URL for website
app.get('/', function(req,res){
    res.sendFile(path.join(__dirname+'/express/index.html'));
    //__dirname : It will resolve to your project folder.
  });

const cassandra = require('cassandra-driver');
const client = new cassandra.Client({ contactPoints: ['10.132.0.23:9042'], localDataCenter: 'datacenter1', keyspace: 'app' });

const query = 'SELECT taux_gaz_effet_serre_cumule, km_parcourus_cumules, avions_en_vol, consommation_maisons FROM current WHERE id=0';

var result;
var resultMap;

var newMap = {
    gasgasgas: 0,
    kmParcourus: 0,
    isItAPlane: 0
}

async function update() {
    result = await client.execute(query);
    resultMap = result.rows[0];

    newMap.gasgasgas = resultMap.taux_gaz_effet_serre_cumule;
    newMap.kmParcourus = resultMap.km_parcourus_cumules;
    newMap.isItAPlane = resultMap.avions_en_vol;
}

const timeInterval = 1000;

app.get('/events', (req, res) => {
  res.writeHead(200, {
      'Content-Type': 'text/event-stream',
      'Cache-Control': 'no-cache'
  });
  setInterval(() => {
      update();
      res.write(`data: ${JSON.stringify(newMap)}\n\n`);
  }, timeInterval)
}
)

const server = https.createServer(app);

const port = 8080;
server.listen(port);console.debug('Server listening on port ' + port);