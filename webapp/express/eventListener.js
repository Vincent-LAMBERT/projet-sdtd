const evtSource = new EventSource("events")

var initialGas;
var initialKm;

evtSource.onmessage = function(event) {
    var newMap = JSON.parse(event.data);

    if (!initialGas) {
        initialGas = newMap.gasgasgas;
        initialKm = newMap.kmParcourus;
    }
    document.getElementById("gasgasgas").innerHTML = Math.round(newMap.gasgasgas - initialGas);
    document.getElementById("kmParcourus").innerHTML = Math.round(newMap.kmParcourus - initialKm);
    document.getElementById("isItAPlane").innerHTML = newMap.isItAPlane;
}