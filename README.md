# Comment lancer notre projet ?

## Producers


Sur chaque machine de production, il faut lancer le script `setup-producerX.sh` placé à la racine du projet.

Dans ces scripts, il y a un `git clone https://gitlab.com/Vincent-LAMBERT/projet-sdtd.git /home/projet-sdtd` qu'il faut remplacer par le repertoire git dans lequel vous avez mis le code, et un seul agent de message kafka est inscrit dans le code. 

Si vous voulez ajouter un agent de message, il faut modifier la ligne `"brokers" : ["kafka.europe-west1-b.c.enhanced-storm-328807.internal:9092", <AUTRE HOSTNAME>]` dans le fichier `producer_state_list.js` qui est dans le sous dossier `src/nodejs`

Ensuite, à chaque lancement des machines de productions, les producers seront lancés.

## Kafka

Pour chaque machine possédant un agent de message de Kafka, il faut lancer le script `setup-kafka.sh`

Ensuite, à chaque lancement des machines possédant un agent de messages de kafka, les instances de kafka et zookeeper seront lancées.

### Utilisation du code Spark.
La commande pour lancer Spark est la suivante `spark-submit --class StreamHandler2 --master local[*] --packages "org.apache.spark:spark-streaming-kafka-0-10_2.12:3.1.2,com.datastax.spark:spark-cassandra-connector_2.12:3.1.0" target/scala-2.12/stream-handler_2.12-1.0.jar`

Pour faire fonctionner le code Spark, il faut renseigner au préalable les adresses IP internes des VM Spark et des nœuds Cassandra. Pour les VM Spark, il faut renseigner les adresses IP dans `kafkaParams`, dans le champ `BOOTSRAP_SERVERS_CONFIG` a la ligne 112. Les 

adresses IP doivent être de la forme suivantes : `“localhost:9092,anotherhost:9092”`
De même, pour les adresses IP Cassandra, il faut modifier la ligne 105. Les adresses IP doivent être de la forme : `"127.0.0.1:9042,192.168.0.1:9051"`.

S’il y a besoin de faire un ajout significatif dans le code, avant d’effectuer la commande spark, il faut build le projet avec la commande sbt packages dans le répertoire où se situe fichier build.sbt

## Cassandra

Pour configurer Cassandra il faut mettre le bon hostname de la VM dans le fichier setup-cassandra.sh ligne 17 : remplacer  cassandra.europe-west1-b.c.enhanced-storm-328807.internal par l'hostname de la VM cassandra. Utiliser l'adresse IP interne de la VM Cassandra à la place de l'hostname marche aussi.

## Application

Pour lancer l'application sur une machine il faut lancer `setup-app.sh`
en remplaçant `git clone https://gitlab.com/Vincent-LAMBERT/projet-sdtd.git /home/projet-sdtd` par le repertoire git dans lequel vous avez mis le code.
