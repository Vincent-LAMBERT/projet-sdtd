import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.SparkConf
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka010._
import com.datastax.spark.connector._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import Array._
import scala.collection.Map._
import scala.collection.mutable.Map
import scala.math._

/*Classe représentant un avion dans les données de Kafka*/
class Avion(icao: String, dates: Double = 0, longitudes: Double = 0, latitudes: Double = 0, vitesses: Double = 0, on_grounds :Boolean = false) extends Serializable {
  var icao24 = icao //id de l'avion
  var date = dates
  var longitude = longitudes
  var latitude = latitudes
  var vitesse = vitesses
  var on_ground = on_grounds

  //Renvoie true si l'avion est au sol
  def isOn_ground(): Int = {
    if (this.on_ground) {
      return 1
    } else {
      return 0
    }
  }
  // Calcule la distance entre un avion et sa derniere apparition en utilisant la formule d'Haversine
  def calculDistance(avionPrecedent: Map[String,List[Double]]): Double= {
    val R = 6371
    val lat1 = avionPrecedent(this.icao24)(1)
    val lat2 = this.latitude
    val lon1 = avionPrecedent(this.icao24)(0)
    val lon2 = this.longitude
    val latDistance = Math.toRadians(lat2 - lat1)
    val lngDistance = Math.toRadians(lon2 - lon1)
    val sinLat = Math.sin(latDistance / 2)
    val sinLng = Math.sin(lngDistance / 2)
    val a = sinLat * sinLat +
      (Math.cos(Math.toRadians(lat2)) *
        Math.cos(Math.toRadians(lat1)) *
        sinLng * sinLng)
    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    return R * c
  }
  //Update le dico avionPrécédent
  def updateMap(avionPrecedent: Map[String,List[Double]]): Unit ={
    if (avionPrecedent.contains(this.icao24)){
      avionPrecedent(this.icao24) = List(this.longitude, this.latitude, this.date, this.vitesse)
    } else {
      avionPrecedent(this.icao24) = List(this.longitude, this.latitude, this.date, this.vitesse)
    }
  }
  //Renvoie la distance et mets a jour le dictionnaire pour un avion
  def getKm1Avion(avionPrecedent: Map[String,List[Double]]): Double ={
    var cptKm: Double = 0
    if (avionPrecedent.contains(this.icao24)) {
      cptKm = this.calculDistance(avionPrecedent)
      this.updateMap(avionPrecedent)
    } else {
      this.updateMap(avionPrecedent)
      return 1
    }
    return cptKm
  }
}


object StreamHandler2 {
  // Dico stockant la derniere apparition des avions
  var avionPrécédent:Map[String,List[Double]] = scala.collection.mutable.Map[String,List[Double]]() /*(icao24->[long,lat,timestamps,vitesse]*/
  var consommation_cumule: Double = 0
  var km_cumule: Double = 0
  // Convertie des string en double (en évitant les valeur null). Utilisé pour parser
  def toDou(string: String): Double ={
    if (string.compareTo("null") == 0){
      return 0
    } else if(string == "" ){
      return 0
    } else {
      return string.toDouble
    }
  }

  /*Enleve les avions dont la derniere info remonte a plus de 15 secondes*/
  def updateAll(timestamp: Double,avionPrecedent: Map[String,List[Double]]): Unit = {
    avionPrecedent.foreach {
      case (icao, liste) =>
        if ((liste(2) + 15) < timestamp) {
          avionPrecedent -= icao
        }
    }
  }




  def main(args: Array[String]) {

    // Création du contexte avec 1 seconde entre chaque batch
    val conf = new SparkConf().setAppName("StreamHandler").set("spark.cassandra.connection.host","cassandra.europe-west1-b.c.enhanced-storm-328807.internal:9042")/*"cassandra.europe-west1-b.c.enhanced-storm-328807.internal:9042"*/
    val ssc = new StreamingContext(conf, Seconds(1))
    val sc = ssc.sparkContext
    sc.setLogLevel("ERROR")
    // Création du stream kafka
    val topicsSet = Set("GeneralPurpose")
    val kafkaParams = Map[String, Object](
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> "kafka.europe-west1-b.c.enhanced-storm-328807.internal:9092",
      ConsumerConfig.GROUP_ID_CONFIG   -> "test",
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer],
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer])
    val messages = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](topicsSet, kafkaParams))
    //Récupère les valeurs du stream
    val value = messages.map(line=>line.value)
    //parsing json
    val parsed_value = value.map(row=>parse(row))
    //Conversion des types
    val convert_value = parsed_value.map(json_row=>(compact(json_row \ "icao"), compact(json_row \ "date"), compact(json_row \ "velocity"), compact(json_row \ "longitude"), compact(json_row \ "latitude"), compact(json_row \ "onGround")))
    //Conversion en instance de la classe Avion
    val avionRdd = convert_value.map(data=>new Avion(data._1,toDou(data._2),toDou(data._4),toDou(data._5),toDou(data._3),data._6.toBoolean))
    //Calcul
    avionRdd.foreachRDD((rdd,time)=>{
      //Avoiding empty batch
      if (!rdd.isEmpty) {
        //Nombre d'avions au sol
        val avionOnGround = rdd.map(avion=>avion.isOn_ground)
        val valeurAvionOnGround = avionOnGround.reduce((x,y)=>x+y)
        print("Nombre d'avions en vol : ")
        print(valeurAvionOnGround)
        //Distance Parcourue
        val kmRdd = rdd.map(x=>x.getKm1Avion(avionPrécédent))
        val km = kmRdd.reduce((x,y)=>x+y)/1000
        km_cumule = km_cumule + km
        print(" km: ")
        print(km)
        //Suppression des avions avec plus de derniere appartion > 15sec
        updateAll(time.milliseconds,avionPrécédent)
        // Consommation
        val consommation = 113*0.150*valeurAvionOnGround*km
        consommation_cumule = consommation_cumule + consommation
        print(" consommation: ")
        print(consommation)
        //Comparaison ménage
        val consommationMaison = consommation_cumule/7800
        print(" nombre de maison: ")
        print(consommationMaison)

        //Sending to Cassandra
        val dataToSend = sc.parallelize(Seq((0,valeurAvionOnGround.toDouble,consommationMaison,km_cumule,time.milliseconds.toDouble,consommation_cumule)))
        dataToSend.saveToCassandra("app","current", SomeColumns("id","avions_en_vol","consommation_maisons","km_parcourus_cumules","last_updated","taux_gaz_effet_serre_cumule"))
        print("Data Send")
        print("\n")
      }
    })
    // Calcul nombre de km parcouru depuis dernier batch
    // Start the computation
    ssc.start()
    ssc.awaitTermination()
  }
}
