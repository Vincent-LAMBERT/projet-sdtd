	name := "Stream Handler"

version := "1.0"

scalaVersion := "2.12.15"
val SPARK_VERSION = "3.1.2"


libraryDependencies ++= Seq(
	"org.apache.spark" %% "spark-core" % SPARK_VERSION  % "provided",
	"org.apache.spark" %% "spark-sql" % SPARK_VERSION  % "provided",
	"org.apache.spark" %% "spark-streaming" % SPARK_VERSION % "provided",
	"org.apache.spark" %% "spark-streaming-kafka-0-10" % SPARK_VERSION,
	"com.datastax.spark" %% "spark-cassandra-connector" % "3.1.0",
	"com.datastax.cassandra" % "cassandra-driver-core" % "3.11.0"


)