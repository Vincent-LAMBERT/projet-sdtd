
DIR="/home/spark/"
if [ -d "$DIR" ]; then
    echo "Starting spark" >> /home/state_history.txt
    # Startup script spark
    export PATH=$PATH:/opt/spark/bin

    cd /home/projet-sdtd/spark/
    spark-submit --class StreamHandler2 --master local[*] --packages "org.apache.spark:spark-streaming-kafka-0-10_2.12:3.1.2,com.datastax.spark:spark-cassandra-connector_2.12:3.1.0" target/scala-2.12/stream-handler_2.12-1.0.jar
else
    echo "Installing spark" >> /home/state_history.txt
    apt-get update && apt-get install -y default-jdk scala git wget
    git clone https://gitlab.com/Vincent-LAMBERT/projet-sdtd.git /home/projet-sdtd
    wget https://dlcdn.apache.org/spark/spark-3.1.2/spark-3.1.2-bin-hadoop3.2.tgz
    tar xvf spark-*
    mv spark-3.1.2-bin-hadoop3.2 /opt/spark
    echo "export SPARK_HOME=/opt/spark" >> /root/.profile
    echo "export PATH=$PATH:/opt/spark/bin:/opt/spark/sbin" >> /root/.profile
    echo "export PYSPARK_PYTHON=/usr/bin/python3" >> /root/.profile
    echo "export SPARK_HOME=/opt/spark" >> /home/vincent_lambert29/.profile
    echo "export PATH=$PATH:/opt/spark/bin:/opt/spark/sbin" >> /home/vincent_lambert29/.profile
    echo "export PYSPARK_PYTHON=/usr/bin/python3" >> /home/vincent_lambert29/.profile
    source ~/.profile
    echo "spark installed" >> /home/state_history.txt
    # Startup script spark
    export PATH=$PATH:/opt/spark/bin
    # Installing sbt
    echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | sudo tee /etc/apt/sources.list.d/sbt.list
    echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | sudo tee /etc/apt/sources.list.d/sbt_old.list
    curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | sudo apt-key add
    sudo apt-get install sbt
    cd /home/projet-sdtd/spark/
    spark-submit --class StreamHandler2 --master local[*] --packages "org.apache.spark:spark-streaming-kafka-0-10_2.12:3.1.2,com.datastax.spark:spark-cassandra-connector_2.12:3.1.0" target/scala-2.12/stream-handler_2.12-1.0.jar
fi
