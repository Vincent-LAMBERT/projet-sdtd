DIR="/home/cassandra/"
if [ -d "$DIR" ]; then
    echo "Starting cassandra" >> /home/state_history.txt
    # Startup script cassandra
else
    echo "Installing cassandra" >> /home/state_history.txt
    apt-get update && apt-get install -y apt-transport-https ca-certificates wget git dirmngr gnupg software-properties-common
    git clone https://gitlab.com/Vincent-LAMBERT/projet-sdtd.git /home/projet-sdtd
    wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | sudo apt-key add -
    add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
    apt-get update && sudo apt-get install -y adoptopenjdk-8-hotspot
    wget -q -O - https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -
    sudo sh -c 'echo "deb https://www.apache.org/dist/cassandra/debian 311x main" > /etc/apt/sources.list.d/cassandra.list'
    apt update && apt install -y cassandra
    echo "cassandra installed" >> /home/state_history.txt
    sudo nodetool drain && sudo systemctl stop cassandra
    sudo sed -i 's/rpc_address: .*/rpc_address: cassandra.europe-west1-b.c.enhanced-storm-328807.internal/'  /etc/cassandra/cassandra.yaml
    sudo systemctl start cassandra
    cqlsh -f /home/projet-sdtd/cql/create_tables.cql 
    echo "cassandra started" >> /home/state_history.txt
fi
