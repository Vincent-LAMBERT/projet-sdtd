
DIR="/home/projet-sdtd/"
if [ -d "$DIR" ]; then
    echo "Starting producer" >> /home/state_history.txt
    # Startup script main producer
    node /home/projet-sdtd/src/nodejs/admin.js
    node /home/projet-sdtd/src/nodejs/producer_state_list.js 0
else
    echo "Installing producer" >> /home/state_history.txt
    apt-get update && apt-get install -y curl git
    git clone https://gitlab.com/Vincent-LAMBERT/projet-sdtd.git /home/projet-sdtd

    apt update && apt upgrade
    curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
    echo "curl done" >> /home/state_history.txt
    apt install -y nodejs
    apt install -y npm
    apt autoremove
    sudo npm install npm
    echo "npm installed" >> /home/state_history.txt
    sudo npm install axios
    echo "axios installed" >> /home/state_history.txt
    sudo npm install kafkajs
    echo "kafkajs installed" >> /home/state_history.txt

    echo "producer installed" >> /home/state_history.txt
    
    # Startup script main producer
    node /home/projet-sdtd/src/nodejs/admin.js
    node /home/projet-sdtd/src/nodejs/producer_state_list.js 0
fi