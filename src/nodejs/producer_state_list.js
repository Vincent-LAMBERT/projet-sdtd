const {Kafka} = require("kafkajs")
var path = require('path');
const process = require("process");
var fetchAPI = require( path.resolve( __dirname, "./fetchApi.js" ) );

var producerType = parseInt(process.argv[2])

let minLongitude = producerType % 2 ? 0.0 : -180.0;
let maxLongitude = producerType % 2 ? 180.0 : 0.0;

let max_time = -1;

async function sendMessage(options) {
    producer = options.producer;
    delay = options.delay;

    var thistime;

    //var lol = new Array(4).fill(0);

    for await([state,time] of fetchAPI.iterateOnStateVectors({
        minLongitude : minLongitude,
        maxLongitude : maxLongitude,
    })){
        thistime = time;
        if(time <= max_time){
            break;
        }
        //console.log("Plane ICAO : " + state[0] + ", date : " + time + ", plane_speed : " + state[9] + " m/s");
        //console.log("Value : " + JSON.stringify(fetchAPI.createValueFromState({time: time, state: state, topic: fetchAPI.kafka_topic.KM_TRAVELED})))
        
        const value = await fetchAPI.createValueFromState({time: time, state: state, topic: fetchAPI.kafka_topic.KM_TRAVELED});
        if(value.longitude != null && value.latitude != null){
            const partition = (value.longitude > 0 ? 0 : 1 ) + (Math.abs(value.longitude) > 80 ? 0 : 1)*2;

            //lol[partition]++;
            console.log("Message to send : " + JSON.stringify(value));
            const result = await producer.send({
                "topic": "GeneralPurpose",
                "messages": [
                    {
                        "value": JSON.stringify(value),
                        "partition": partition
                    }
                ]
            });

            console.log("Message sent ?");
        }
    }
    if(thistime > max_time){
        max_time = thistime;
    }
    //console.log(thistime, producerType, lol);
    setTimeout(() => sendMessage(options), delay);
}


async function run(){
    try{  
        const kafka = new Kafka({
            "clientId": "uwu",
            "brokers" : ["kafka.europe-west1-b.c.enhanced-storm-328807.internal:9092"] 
        })

        const producer = kafka.producer();
        console.log("Connecting.....");
        await producer.connect();
        console.log("Connected !");
        
        await sendMessage({
            delay : 50,
            producer : producer
        });

    }
    catch(e){
        console.error(`UwU shit happens ${e.message}`);
    }
}

run().catch(e => console.error(`UwU shit happens ${e.message}`, e));


const signalTraps = ['SIGTERM', 'SIGINT', 'SIGUSR2']

signalTraps.map(type => {
    process.once(type, async () => {
      try {
        await producer.disconnect();
      } finally {
        process.kill(process.pid, type);
      }
    });
  });