const axios = require('axios');

const kafka_topic = {
    GREENHOUSE_GAZ : 'greenhouse_gaz',
    KM_TRAVELED : 'km_traveled',
    PLANE_NUMBER : 'plane_number'
}

module.exports = {
    iterateOnStateVectors: iterateOnStateVectors,
    createValueFromState: createValueFromState,
    kafka_topic: kafka_topic
}

function getAllStateVectors(option){
    minLatitude = option.minLatitude ?? null;
    maxLatitude = option.maxLatitude ?? null;
    minLongitude = option.minLongitude ?? null;
    maxLongitude = option.maxLongitude ?? null;
    if(minLatitude || maxLatitude || minLongitude || maxLongitude){
        minLatitude = option.minLatitude ?? -180.0;
        maxLatitude = option.maxLatitude ?? 180.0;
        minLongitude = option.minLongitude ?? -180.0;
        maxLongitude = option.maxLongitude ?? 180.0;
    }

    let firstAdd = true;
    var URL = 'https://opensky-network.org/api/states/all';
    if(minLatitude != null) [URL, firstAdd] = addToURL(URL,"lamin=" + minLatitude, firstAdd);
    if(minLongitude != null) [URL, firstAdd] = addToURL(URL,"lomin=" + minLongitude, firstAdd);
    if(maxLatitude != null) [URL, firstAdd] = addToURL(URL,"lamax=" + maxLatitude, firstAdd);
    if(maxLongitude != null) [URL, firstAdd] = addToURL(URL,"lomax=" + maxLongitude, firstAdd);

    console.log(URL);
    
    return axios.get(URL);
}

function addToURL(URL, str, firstAdd){
    if(firstAdd){
        URL += '?' + str;
    } else {
        URL += '&' + str;
    }
    return [URL, false];
}


async function* iterateOnStateVectors(option){
    try{
        const response = await getAllStateVectors(option);
        var time = response.data.time;
        var states = response.data.states;
        for(const state of states){
            yield [state,time];
        }
    }catch(error)
    {
        console.log(error);
    }
}

function createValueFromState(option){
    let state = option.state;
    let time = option.time;
    let topic = option.topic ?? kafka_topic.GREENHOUSE_GAZ;

    let value = {icao: state[0], date: time};
    if(topic != kafka_topic.PLANE_NUMBER){
        value.velocity = state[9]
        if(topic == kafka_topic.KM_TRAVELED){
            value.longitude = state[5]
            value.latitude = state[6]
            value.onGround = state[8]
        }
    }
    return value;
}





// getAllStateVectors().then(response => {
    
//     // Get JSON data
//     console.log(response.data.time);
//   })
//   .catch(error => {
//     console.log(error);
//   });
