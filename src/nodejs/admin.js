const {Kafka} = require("kafkajs")

run();


async function run(){
    try{  
        const kafka = new Kafka({
            "clientId": "uwu",
            "brokers" : ["kafka.europe-west1-b.c.enhanced-storm-328807.internal:9092    "] 
        })

        const admin = kafka.admin();
        console.log("Connecting.....")
        await admin.connect()
        console.log("Connected !")

        let topics = await admin.listTopics();

        console.log(`Existing topics : ${topics}`)

        await createTopic({
            admin : admin,
            topic : 'GeneralPurpose',
            numPartitions : 4,
            existingTopics : topics
        })

        // await createTopic({
        //     admin : admin,
        //     topic : 'Arrivals',
        //     numPartitions : 1,
        //     existingTopics : topics
        // })

        // await createTopic({
        //     admin : admin,
        //     topic : 'Departures',
        //     numPartitions : 1,
        //     existingTopics : topics
        // })


        // // Delete all topics
        // await admin.deleteTopics({
        //     topics : topics
        // })

        await admin.disconnect()


    }
    catch(error){
        console.error(`UwU shit happens` + error)
    }
    finally{
        process.exit(0);
    }
}

async function createTopic(options){
    let admin = options.admin;
    let topic = options.topic;
    let numPartitions = options.numPartitions ?? 1;
    let existingTopics = options.existingTopics ?? await admin.listTopics();
    try{  
        if(!existingTopics.find(element => element == topic)) {
            await admin.createTopics({
                "topics": [{
                    "topic" : topic,
                    "numPartitions": numPartitions
                }]
            });

            console.log("Topic "+ topic +" created successfully")
        } else {
            console.log("Topic "+ topic +" already exists")
        }
    }
    catch(error){
        console.error(`Error in createTopic: ${error}`)
    }
}