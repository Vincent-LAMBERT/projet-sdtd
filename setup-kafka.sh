
DIR="/home/kafka/"
if [ -d "$DIR" ]; then
    echo "Starting kafka" >> /home/state_history.txt
    
    # Startup script spark
    ./home/kafka/kafka/bin/zookeeper-server-start.sh /home/kafka/kafka/config/zookeeper.properties
    ./home/kafka/kafka/bin/kafka-server-start.sh /home/kafka/kafka/config/server.properties
else
    echo "Installing kafka" >> /home/state_history.txt
    apt-get update && apt-get install -y curl default-jdk default-jre
    mkdir /home/kafka
    mkdir /home/kafka/kafka
    curl "https://archive.apache.org/dist/kafka/2.1.1/kafka_2.11-2.1.1.tgz" -o /home/kafka/kafka.tgz
    tar -xvzf /home/kafka/kafka.tgz --strip 1 -C /home/kafka/kafka
    rm /home/kafka/kafka.tgz
    echo   >> /home/kafka/kafka/config/server.properties
    echo delete.topic.enable = true >> /home/kafka/kafka/config/server.properties
    echo "kafka installed" >> /home/state_history.txt
    # Startup systemctl service doesn't work
    # echo [Unit] > /etc/systemd/system/zookeeper.service
    # echo Requires=network.target remote-fs.target >> /etc/systemd/system/zookeeper.service
    # echo After=network.target remote-fs.target >> /etc/systemd/system/zookeeper.service
    # echo [Service] >> /etc/systemd/system/zookeeper.service
    # echo Type=simple >> /etc/systemd/system/zookeeper.service
    # echo User=root >> /etc/systemd/system/zookeeper.service
    # echo ExecStart= /home/kafka/kafka/bin/zookeeper-server-start.sh /home/kafka/kafka/config/zookeeper.properties >> /etc/systemd/system/zookeeper.service
    # echo ExecStop=/home/kafka/kafka/bin/zookeeper-server-stop.sh >> /etc/systemd/system/zookeeper.service
    # echo Restart=on-abnormal >> /etc/systemd/system/zookeeper.service
    # echo [Install] >> /etc/systemd/system/zookeeper.service
    # echo WantedBy=multi-user.target >> /etc/systemd/system/zookeeper.service

    # echo [Unit] > /etc/systemd/system/kafka.service
    # echo Description=Apache Kafka Server >> /etc/systemd/system/kafka.service
    # echo Documentation=http://kafka.apache.org/documentation.html >> /etc/systemd/system/kafka.service
    # echo Requires=zookeeper.service >> /etc/systemd/system/kafka.service
    # echo [Service] >> /etc/systemd/system/kafka.service
    # echo Type=simple >> /etc/systemd/system/kafka.service
    # echo Environment="JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64" >> /etc/systemd/system/kafka.service
    # echo ExecStart= /home/kafka/kafka/bin/kafka-server-start.sh /home/kafka/kafka/config/server.properties >> /etc/systemd/system/kafka.service
    # echo ExecStop=/home/kafka/kafka/bin/kafka-server-stop.sh >> /etc/systemd/system/kafka.service
    # echo Restart=on-abnormal >> /etc/systemd/system/kafka.service
    # echo [Install] >> /etc/systemd/system/kafka.service
    # echo WantedBy=multi-user.target >> /etc/systemd/system/kafka.service

    # systemctl daemon-reload
    # systemctl start kafka
    # systemctl enable kafka

    # Startup script spark
    ./home/kafka/kafka/bin/zookeeper-server-start.sh /home/kafka/kafka/config/zookeeper.properties
    ./home/kafka/kafka/bin/kafka-server-start.sh /home/kafka/kafka/config/server.properties
fi