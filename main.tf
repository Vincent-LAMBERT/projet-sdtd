terraform { 
  required_providers {    
    google = {
      version = "~> 3.90.0"
      source = "hashicorp/google"    
    }  
  }
}

#===========#
# VARIABLES #
#===========#

variable "gcp_project_id" {
  type = string
  sensitive = false
  description = "Google Cloud project ID"
}

variable "gcp_credentials" {
  type = string
  sensitive = true
  description = "Google Cloud service account credentials"
}

variable "gcp_region" {
  type = string
  sensitive = false
  description = "Google Cloud region"
}

variable "gcp_zone" {
  type = string
  sensitive = false
  description = "Google Cloud zone"
}

variable "server_manual_on" {
  type = bool
  description = "deploy instances on true"
  default = true
}

variable "server_auto_on" {
  type = bool
  description = "deploy auto instances on true"
  default = false
}

#===========#
# PROVIDERS #
#===========#

provider "google" {
  project = var.gcp_project_id
  credentials = var.gcp_credentials
  region = var.gcp_region
  zone = var.gcp_zone
}

#===========#
# RESOURCES #
#===========#

resource "google_compute_instance" "vm_producer1" {
  name         = "producer1"
  machine_type = "f1-micro"
  allow_stopping_for_update = "true"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  desired_status = (var.server_manual_on == true ? "RUNNING" : "TERMINATED")
  can_ip_forward = true
}

resource "google_compute_instance" "vm_producer_auto_1" {
  name         = "producer-auto-1"
  machine_type = "f1-micro"
  allow_stopping_for_update = "true"
  metadata_startup_script = "${file("${path.module}/setup-producer1.sh")}"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  desired_status = (var.server_auto_on == true ? "RUNNING" : "TERMINATED")
}

# resource "google_compute_instance" "vm_producer_auto_2" {
#   name         = "producer-auto-2"
#   machine_type = "f1-micro"
#   allow_stopping_for_update = "true"
#   metadata_startup_script = "${file("${path.module}/setup-producer2.sh")}"

#   boot_disk {
#     initialize_params {
#       image = "debian-cloud/debian-10"
#     }
#   }

#   network_interface {
#     network = "default"
#     access_config {
#     }
#   }
#   desired_status = (var.server_auto_on == true ? "RUNNING" : "TERMINATED")
# }


resource "google_compute_instance" "vm_producer2" {
  name         = "producer2"
  machine_type = "f1-micro"
  allow_stopping_for_update = "true"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  desired_status = (var.server_manual_on == true ? "RUNNING" : "TERMINATED")
  can_ip_forward = true
}
 

resource "google_compute_instance" "vm_kafka" {
  name         = "kafka"
  machine_type = "e2-small" # JRE needs more than 1GB to run
  allow_stopping_for_update = "true"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    # access_config {
    # }
  }
  desired_status = (var.server_manual_on == true ? "RUNNING" : "TERMINATED")
}


resource "google_compute_instance" "vm_kafka_auto_1" {
  name         = "kafka-auto-1"
  machine_type = "e2-small" # JRE needs more than 1GB to run
  allow_stopping_for_update = "true"
  metadata_startup_script = "${file("${path.module}/setup-kafka.sh")}"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    # access_config {
    # }
  }
  desired_status = (var.server_auto_on == true ? "RUNNING" : "TERMINATED")
}

resource "google_compute_instance" "vm_kafka_auto_2" {
  name         = "kafka-auto-2"
  machine_type = "e2-small" # JRE needs more than 1GB to run
  allow_stopping_for_update = "true"
  metadata_startup_script = "${file("${path.module}/setup-kafka.sh")}"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    # access_config {
    # }
  }
  desired_status = (var.server_auto_on == true ? "RUNNING" : "TERMINATED")
}
resource "google_compute_instance" "vm_spark" {
  name         = "spark"
  machine_type = "e2-medium"
  allow_stopping_for_update = "true"

  tags = ["http-server","https-server"]
  metadata = {
    ssh-keys : <<EOT
  quentin_scabello:ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAu4P85aOZmcvCzTFP32zqIzbtcNAuaq/iJ74Os7aUyE6yXVbaKSzzD4bcOvfGOQbc2Gx60beG+GKFkrWD2spjE= google-ssh {"userName":"quentin.scabello@gmail.com","expireOn":"2022-01-14T12:07:56+0000"}
  EOT 
  }

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  desired_status = (var.server_manual_on == true ? "RUNNING" : "TERMINATED")
}

resource "google_compute_instance" "vm_spark_auto_1" {
  name         = "spark-auto-1"
  machine_type = "e2-medium"
  allow_stopping_for_update = "true"
  metadata_startup_script = "${file("${path.module}/setup-spark.sh")}"

  tags = ["http-server","https-server"]
  metadata = {
    ssh-keys : <<EOT
  quentin_scabello:ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAu4P85aOZmcvCzTFP32zqIzbtcNAuaq/iJ74Os7aUyE6yXVbaKSzzD4bcOvfGOQbc2Gx60beG+GKFkrWD2spjE= google-ssh {"userName":"quentin.scabello@gmail.com","expireOn":"2022-01-14T12:07:56+0000"}
  EOT 
  }

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  desired_status = (var.server_auto_on == true ? "RUNNING" : "TERMINATED")
}

resource "google_compute_instance" "vm_spark_auto_2" {
  name         = "spark-auto-2"
  machine_type = "e2-medium"
  allow_stopping_for_update = "true"
  metadata_startup_script = "${file("${path.module}/setup-spark.sh")}"

  tags = ["http-server","https-server"]
  metadata = {
    ssh-keys : <<EOT
  quentin_scabello:ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAu4P85aOZmcvCzTFP32zqIzbtcNAuaq/iJ74Os7aUyE6yXVbaKSzzD4bcOvfGOQbc2Gx60beG+GKFkrWD2spjE= google-ssh {"userName":"quentin.scabello@gmail.com","expireOn":"2022-01-14T12:07:56+0000"}
  EOT 
  }

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  desired_status = (var.server_auto_on == true ? "RUNNING" : "TERMINATED")
}

resource "google_compute_instance" "vm_spark_auto_3" {
  name         = "spark-auto-3"
  machine_type = "e2-medium"
  allow_stopping_for_update = "true"
  metadata_startup_script = "${file("${path.module}/setup-spark.sh")}"

  tags = ["http-server","https-server"]
  metadata = {
    ssh-keys : <<EOT
  quentin_scabello:ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAu4P85aOZmcvCzTFP32zqIzbtcNAuaq/iJ74Os7aUyE6yXVbaKSzzD4bcOvfGOQbc2Gx60beG+GKFkrWD2spjE= google-ssh {"userName":"quentin.scabello@gmail.com","expireOn":"2022-01-14T12:07:56+0000"}
  EOT 
  }

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  desired_status = (var.server_auto_on == true ? "RUNNING" : "TERMINATED")
}

resource "google_compute_instance" "vm_cassandra" {
  name         = "cassandra"
  machine_type = "e2-medium" # JRE needs more than 1GB to run
  allow_stopping_for_update = "true"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
  }
  desired_status = (var.server_manual_on == true ? "RUNNING" : "TERMINATED")
}
 
resource "google_compute_instance" "vm_cassandra_auto_1" {
  name         = "cassandra-auto-1"
  machine_type = "e2-medium" # JRE needs more than 1GB to run
  allow_stopping_for_update = "true"
  metadata_startup_script = "${file("${path.module}/setup-cassandra.sh")}"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
  }
  desired_status = (var.server_auto_on == true ? "RUNNING" : "TERMINATED")
}
 
resource "google_compute_instance" "vm_cassandra_auto_2" {
  name         = "cassandra-auto-2"
  machine_type = "e2-medium" # JRE needs more than 1GB to run
  allow_stopping_for_update = "true"
  metadata_startup_script = "${file("${path.module}/setup-cassandra.sh")}"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
  }
  desired_status = (var.server_auto_on == true ? "RUNNING" : "TERMINATED")
}

resource "google_compute_instance" "vm_app" {
  name         = "app"
  machine_type = "f1-micro"
  allow_stopping_for_update = "true"

  tags = ["http-server","https-server"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  desired_status = (var.server_manual_on == true ? "RUNNING" : "TERMINATED")
}

resource "google_compute_instance" "vm_app_auto" {
  name         = "app-auto"
  machine_type = "f1-micro"
  allow_stopping_for_update = "true"
  metadata_startup_script = "${file("${path.module}/setup-app.sh")}"

  tags = ["http-server","https-server"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  desired_status = (var.server_auto_on == true ? "RUNNING" : "TERMINATED")
}

# Permet de save la dernière config de spark
resource "google_compute_snapshot" "snap-spark" {
  name        = "snap-spark"
  source_disk = "spark"
}
# Permet de save la dernière config de kafka
resource "google_compute_snapshot" "snap-kafka" {
  name        = "snap-kafka"
  source_disk = "kafka"
}

# duplication

# resource "google_compute_disk" "kafka-snap-disk" {
#   name  = "kafka-snap-disk"
#   type  = "pd-ssd"
#   snapshot = "snap-kafka"
#   depends_on = [google_compute_snapshot.snap-kafka]
# }

# resource "google_compute_instance" "vm_kafkabis" {
#   name         = "kafkabis"
#   machine_type = "e2-small" # JRE needs more than 1GB to run
#   allow_stopping_for_update = "true"

#   boot_disk {
#     source = "kafka-snap-disk"
#   }

#   network_interface {
#     network = "default"
#     access_config {
#     }
#   }
#   depends_on = [google_compute_disk.kafka-snap-disk]
# }