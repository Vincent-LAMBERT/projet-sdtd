
DIR="/home/projet-sdtd/"
if [ -d "$DIR" ]; then
    echo "Starting app" >> /home/state_history.txt
else
    echo "Installing app" >> /home/state_history.txt
    apt-get update && apt-get install -y curl git
    git clone https://gitlab.com/Vincent-LAMBERT/projet-sdtd.git /home/projet-sdtd
    curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
    apt-get install -y nodejs
    sudo npm i -g pm2 
    sudp pm2 startup
    sudo pm2 start /home/projet-sdtd/webapp/server.js -n api-service-staging
    sudo pm2 save
    echo "app installed" >> /home/state_history.txt
fi